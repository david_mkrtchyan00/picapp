class Keys:
    email_service = "email service"
    message_db_path = "message db path"
    db_path = "db path"
    generate_emails_checkbox = "generate emails checkbox"
    attachments_paths = 'attachments paths'

storage_keys = Keys()

storage_default_values = {
    "email service": "outlook",
    "generate emails checkbox": True,
    "db path": "",
    "message db path": "",
    "attachments paths": ""
}

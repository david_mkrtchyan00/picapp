import os

database_folder = os.path.join("database")
stored_by_company_folder = os.path.join(database_folder, "stored by company")
user_store_folder = os.path.join("store")
cache_folder = os.path.join("cache")
config_folder = os.path.join("config")
feedback_folder = os.path.join("feedback")
user_store = os.path.join(user_store_folder, "userConfigs.json")

app_log = os.path.join(cache_folder, "app.log")

errors_file = os.path.join(cache_folder, 'errors.json')


message_db_file = os.path.join(cache_folder, 'metadata.db')

email_format_db = os.path.join(cache_folder, 'email_format_db.db')

chromedriver_file = os.path.join(cache_folder, 'chromedriver.exe')

# configs

company_name = os.path.join(config_folder, 'company_name.txt')

company_info = os.path.join(config_folder, 'info.txt')

not_allowed_names = os.path.join(config_folder, 'not_allowed_names.txt')

extract_from = os.path.join(config_folder, 'extract_from.txt')

# data 

email_template = os.path.join('email config', 'email.docx')

sender_accounts = os.path.join(config_folder, 'accounts.xlsx')



import datetime


def epoch_to_strftime(epoch_time):
    """
    it's taking epoch time and returning time in str format
    e.g. 1684335185 -> 06:53 PM 02/18/2023
    :param epoch_time:
    :return:
    """
    converted_time = datetime.datetime.fromtimestamp(epoch_time).strftime("%I:%M %p %m/%d/%Y")
    return converted_time

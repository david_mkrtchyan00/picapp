import os
from constants import paths

required_folders = [
    paths.database_folder, 
    paths.user_store_folder, 
    paths.cache_folder, 
    paths.config_folder, 
    paths.feedback_folder
    ]

def setup():
    for folder in required_folders:
        if not os.path.exists(folder):
            os.makedirs(folder)

#!/bin/bash
echo Start setup
if [[ "$OSTYPE" == "msys" ]]; then
    echo Creating virtual environment...
    python -m venv ../venv || echo Failed
    echo Activate venv...
    source ../venv/Scripts/activate || echo Failed
    echo Installig requirements...
    pip install -r requirements.txt || echo Failed
    echo Succesfully finished 
else 
    echo Not supported platform
fi

read -p "Press enter to continue" x
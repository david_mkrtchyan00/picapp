import logging
import traceback
import openpyxl
from openpyxl.styles import PatternFill
from PyQt5.QtCore import QThread, pyqtSignal  

from store import UserStore
from constants import storage_keys
from .meta_db import MetaDataDB
from helpers.time_converter import epoch_to_strftime


class UpdateMailingList(QThread):
    exception_signal = pyqtSignal(str, str)
    user_store = UserStore()

    def run(self):
        self.meta_db = MetaDataDB()
        msg_db_path = self.user_store.get(storage_keys.message_db_path)
        sent_emails = self.meta_db.get_active_db_sent_emails(msg_db_path, is_dict_format=True)
        try:
            database_file = openpyxl.load_workbook(msg_db_path)
            ws0 = database_file.worksheets[0]
            database_records_size = ws0.max_row
            if len(database_file.worksheets) < 2:
                logging.info('UpdateMailingList:run. No mailing list, copy for mailing list')
                ws = database_file.copy_worksheet(ws0) 
                ws.title = 'Mailing list'
                # database_file.create_sheet('Mailing list')
            else:
                ws = database_file.worksheets[1]
                mailinglist_records_size = ws.max_row
                if database_records_size > mailinglist_records_size:
                    database_file.remove_sheet(ws)
                    logging.info('UpdateMailingList:run. Database updated, copy for mailing list')
                    ws = database_file.copy_worksheet(ws0)
                    ws.title = 'Mailing list'  

            color1 = PatternFill(
                start_color='1388ec',
                end_color='1388ec',
                fill_type='solid'
            )

            color2 = PatternFill(
                start_color='bcd32c',
                end_color='bcd32c',
                fill_type='solid'
            )
            j = 0
            for i, (key_email, sent_email_list) in enumerate(sent_emails.items()):
                row_number = i + 1
                # TODO: instead of sent emails must be whole db size(to make sure no losed any)
                try:
                    if (j // 495) % 2 == 0:
                        choosed_color = color1
                    else:
                        choosed_color = color2

                    email_node_value = ws[f'C{row_number}'].value
                    if email_node_value and email_node_value in sent_emails:
                        ws[f'A{row_number}'].fill = choosed_color
                        ws[f'B{row_number}'].fill = choosed_color
                        ws[f'C{row_number}'].fill = choosed_color

                        # add sent and received emails
                        column_d = 4
                        column_e = 5  # number od the column
                        sent_date = sent_emails[email_node_value][-1]
                        ws.cell(row=row_number, column=column_d).value = sent_emails[email_node_value][1]
                        ws.cell(row=row_number, column=column_e).value = \
                            epoch_to_strftime(sent_date)
                        # change the color of this fields
                        ws[f'D{row_number}'].fill = choosed_color
                        ws[f'E{row_number}'].fill = choosed_color

                except Exception as e:
                    logging.info('UpdateMailingList:run. fail to show sent email in mailing list', exc_info=True)
                j += 1
            database_file.save(msg_db_path)
            database_file.close()
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Please make sure databases are available and closed')

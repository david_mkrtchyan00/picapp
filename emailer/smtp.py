import os
import time 
import logging

import traceback
import docx2txt
from PyQt5.QtCore import QThread, pyqtSlot, pyqtSignal  
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import smtplib
import imaplib

import configs
from emailer.helper import EmailHelper
from constants import email_template, storage_keys
from store import UserStore
from .meta_db import MetaDataDB

class SendEmailSMTP(QThread):
    user_store = UserStore()
    email_helper = EmailHelper()
    exception_signal = pyqtSignal(str, str)
    sent_count_signal = pyqtSignal(int, int)
    stop = False

    @pyqtSlot(bool)
    def stop_signal(self,stop):
        self.stop=stop

    def initial(self):
        self.meta_db = MetaDataDB()
        self.sender_accounts = configs.get_sender_accounts()
        self.active_msg_db_path = self.user_store.get(storage_keys.message_db_path) 

    def run(self):
        try:
            self.initial()
            self.get_email_attributes()
            self.get_employees_list()
            
            for account_position, sender_account in enumerate(self.sender_accounts):
                sent_count = 0
                self.login(sender_account)
                logging.info(f'SendEmailSMTP:run. Sending from {sender_account["email"]}')
                for employee in self.employees_info_list[account_position]:
                    if not employee:
                        print('Already sent all employees')
                        return 
                    if self.stop:
                        logging.info('SendEmailSMTP:run. Stoped manually')
                        return
                    self.generate_email_template(employee)
                    self.create_email(sender_account, employee)
                    self.attach_files()
                    self.send()
                    time.sleep(0.1)
                    sent_count += 1
                    self.meta_db.add_emailed_data(
                        self.active_msg_db_path, 
                        sender_account['email'], 
                        employee['email']
                        )
                    print('Sender:', sender_account['email'], 'Sent:', employee['email'], ' | ',
                          'Sent emails count:', sent_count)
                    self.sent_count_signal.emit(sent_count, account_position)
                self.logout()
                
        except:
            tb = str(traceback.format_exc())
            print(tb)
            self.exception_signal.emit(tb, ('There is technical problem.'
                                            ' Please stop and try later'))

    def send(self):
        text = self.msg.as_string()
        self.server.sendmail(self.msg['From'], self.msg['To'], text)
        self.imap.append('Sent', '\\Seen', imaplib.Time2Internaldate(time.time()), text.encode('utf8')) 

    def create_email(self, sender_account, employee):  
        self.msg = MIMEMultipart()
        self.msg['From'] = sender_account['email']
        self.msg['To'] = employee['email']
        self.msg['Subject'] = self.subject
        self.msg.attach(MIMEText(self.message, 'html'))

    def generate_email_template(self, employee_info):
        self.subject = self.subject_tmp.replace(
            '{{first_name}}', employee_info['first_name']).replace(
                '{{company}}', employee_info['company']).replace(
                    '{{email}}', employee_info['email'])
        self.message = self.message_tmp.replace(
            '{{first_name}}', employee_info['first_name']).replace(
                '{{company}}', employee_info['company']).replace(
                    '{{email}}', employee_info['email'])

    def attach_files(self):
        if not self.attachments:
            return
        for attachmentPath in self.attachments:
            file_type = attachmentPath.split('.')[-1]
            filename = os.path.basename(attachmentPath)
            with open(attachmentPath, "rb") as attachment:
                p = MIMEApplication(attachment.read(),_subtype=file_type)	
                p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
                self.msg.attach(p)

    def get_email_attributes(self):
        full_message = docx2txt.process(email_template)
        if 'Message:' in full_message:
            message_list = full_message.split('Message:')
        else:
            message_list = ['', full_message]
        message_list[0] = message_list[0].strip('\n')
        message_list[1] = message_list[1].strip('\n')
        self.subject_tmp = message_list[0]
        self.message_tmp = message_list[1]
        self.attachments = self.user_store.get(storage_keys.attachments_paths)

    def get_employees_list(self):
        employees = self.email_helper.get_employees_dict(self.active_msg_db_path)
        sent_emails_from_active_db = self.meta_db.get_active_db_sent_emails(self.active_msg_db_path)

        sender_emails_day_activity = []
        for sender_account in self.sender_accounts:
            sent_emails_count = self.meta_db.get_required_email_day_activity(self.active_msg_db_path, sender_account['email'])
            sender_emails_day_activity.append({'email': sender_account['email'], 'count': sent_emails_count})

        self.employees_info_list = self.email_helper.get_employees_sorted_by_sender_emails(
            employees,
            sender_emails_day_activity,
            sent_emails_from_active_db
            )
        
    def login(self, sender_account):
        password = "dmpictury2022"
        hostname = sender_account['host_name']
        logging.info(f'SendEmailSMTP:login. using host {hostname} with email {sender_account["email"]}')
        self.server = smtplib.SMTP(f'smtp.{hostname}.net: 587')
        self.server.starttls()
        self.server.login(sender_account['email'], sender_account['password'])
        self.imap = imaplib.IMAP4_SSL(f'imap.{hostname}.net')
        self.imap.login(sender_account['email'], sender_account['password'])

    def logout(self):
        self.imap.logout()
        self.server.quit()

import os
import openpyxl
import logging

class EmailHelper:
    def get_employees(self, db_path):
        has_old_data = os.path.isfile(db_path)
        if not has_old_data:
            return []
        book = openpyxl.load_workbook(db_path, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = sheet.rows
        key_list = ['first_name', 'company', 'email', 'feedback']
        len_columns = len(key_list)
        employees_database = []
        for row in rows:
            values_dict = {}
            for i, cell in enumerate(row):
                if i >= len_columns:
                    continue
                if cell.value:
                    values_dict[key_list[i]] = cell.value
                else:
                    values_dict[key_list[i]] = ''
            employees_database.append(values_dict)
        book.close()
        return employees_database

    def manage_employees(self, employees_database, sender_accounts, sent_emails_current_day_count, sent_emails_count):
        #TODO: add config for 495 number (max sent email count one in day from one email)
        to_send_emails = employees_database[sent_emails_count:]
        employees_info_list = list()
        for i in range(1, len(sender_accounts) + 1):
            if sent_emails_current_day_count > 495:
                one_part_of_accounts = []
                sent_emails_current_day_count -= 495
            else:
                if i > 1:
                    start_from = ((i - 1) * 495) - sent_emails_current_day_count
                else:
                    start_from = (i - 1) * 495
                end_to=(i * 495) - sent_emails_current_day_count
                one_part_of_accounts = to_send_emails[start_from: end_to]  # 495 2 hat
            
            print(len(one_part_of_accounts), ' emails should send now')
            employees_info_list.append(one_part_of_accounts)  # [[{}],[{}],[{}]]
        return employees_info_list
    
    def get_employees_dict(self, db_path):
        has_old_data = os.path.isfile(db_path)
        if not has_old_data:
            return {}
        book = openpyxl.load_workbook(db_path, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        data = {}
        for row in sheet.iter_rows(values_only=True):
            if len(row) >= 3:
                first_name = row[0]
                company = row[1]
                email = row[2]
                data[email] = {'first_name': first_name, 'company': company, 'email': email}

        return data

    def get_employees_sorted_by_sender_emails(self, employees, sender_emails_day_activity, sent_emails_from_active_db):
        for sent_email_list in sent_emails_from_active_db:
            sent_email = sent_email_list[2]
            employees.pop(sent_email, '')
        
        employees_list = list(employees.values())

        employees_by_emails = []
        for i, sender_email_dict in enumerate(sender_emails_day_activity):
            slice_end_to = 495 - sender_email_dict['count']
            employees_for_each_sender = employees_list[0: slice_end_to]
            del employees_list[0: slice_end_to]
            employees_by_emails.append(employees_for_each_sender)
            logging.info(f'EmailHelper:get_employees_sorted_by_sender_emails. To send count: {len(employees_for_each_sender)} email: {sender_email_dict["email"]}')
        return employees_by_emails  # [[{}],[{}],[{}]]
import sqlite3
import time
import logging

from constants import message_db_file

ONE_DAY_IN_SECONDS = 86400

class MetaDataDB:
    conn, cur, = None, None
    def connect_msg_db(self):
        logging.info('MetaDataDB:connect_msg_db. connecting to metdata db')
        self.conn = sqlite3.connect(message_db_file)
        self.cur = self.conn.cursor()
        query = f"""CREATE TABLE IF NOT EXISTS metadata (
                                            db_name VARCHAR(255),
                                            sender VARCHAR(255),
                                            send_to VARCHAR(255),
                                            unix_time int);"""
        self.cur.execute(query)

    def _get_db_data(self, query, args):
        if not self.conn or not self.cur:
            self.connect_msg_db()
        try:
            self.cur.execute(query, args)
            metadata = self.cur.fetchall()
            send_emails_count = metadata[0][0]
        except Exception as e:
            send_emails_count = 0
            logging.error('MetaDataDB:_get_db_data.', exc_info=True)
        return send_emails_count
    
    def _create_dict_by_email_key(self, sent_emails):
        sent_emails_dict = {}
        for row_tuple in sent_emails:
            email_index = 2
            sent_emails_dict[row_tuple[email_index]] = list(row_tuple)
        return sent_emails_dict

    
    def get_active_db_activity(self, db_name):
        # get selected db sent emails count
        if (not db_name):
            logging.info(f'MetaDataDB:get_active_db_activity. db_name: {db_name}, is empty')
            return 0
        # logging.info(f'MetaDataDB:get_active_db_activity. Get db_name: {db_name} activity')
        query = f"SELECT COUNT(*) FROM metadata WHERE db_name = ?"
        send_emails_count = self._get_db_data(query, (db_name,))
        return send_emails_count
    
    def get_active_db_day_activity(self, db_name):
        # get selected db last 24 hour sent emails count
        if (not db_name):
            logging.info(f'MetaDataDB:get_active_db_day_activity. db_name: {db_name}, is empty')
            return 0
        # logging.info(f'MetaDataDB: get_active_db_day_activity. get db_name: {db_name} activity')
        last_day_unix_time = int(time.time()) - ONE_DAY_IN_SECONDS
        query = f"SELECT COUNT(*) FROM metadata WHERE db_name = ? and unix_time > ?"
        send_emails_count = self._get_db_data(query, (db_name, last_day_unix_time))
        return send_emails_count
    
    def get_required_email_day_activity(self, db_name, sender):
        # get required email last 24 hour sent emails count
        if (not db_name or not sender):
            logging.info(f'MetaDataDB:get_required_email_day_activity. db_name: {db_name}, or sender: {sender}, is empty')
            return 0
        # logging.info(f'MetaDataDB:get_required_email_day_activity. Get db_name: {db_name}, sender: {sender} activity')
        last_day_unix_time = int(time.time()) - ONE_DAY_IN_SECONDS
        query = f"SELECT COUNT(*) FROM metadata WHERE sender = ? and unix_time > ?"
        send_emails_count = self._get_db_data(query, (sender, last_day_unix_time,))
        return send_emails_count
    
    def get_active_db_sent_emails(self, db_name, is_dict_format=False):
        if not (db_name):
            logging.info(f'MetaDataDB:get_active_db_sent_emails. db_name: {db_name}, is empty')
            return []
        if not self.conn or not self.cur:
            self.connect_msg_db()
        # logging.info(f'MetaDataDB:get_active_db_sent_emails. Get db_name: {db_name} activity')
        query = f"SELECT * FROM metadata WHERE db_name = ?"
        self.cur.execute(query, (db_name,))
        sent_emails = self.cur.fetchall()
        if is_dict_format:
            sent_emails = self._create_dict_by_email_key(sent_emails)
        return sent_emails
        

    def add_emailed_data(self, db_name, sender, send_to):
        if (not db_name or not sender or not send_to):
            logging.error(f'MetaDataDB:add_emailed_data.  db_name: {db_name}, or sender: {sender}, or send_to: {send_to}, is empty')
            return
        if not self.conn or not self.cur:
            self.connect_msg_db()
        # logging.debug('MetaDataDB:add_emailed_data. db_name: {db_name}, sender: {sender}, send_to: {send_to}')
        message_info = (db_name, sender, send_to, int(time.time()))
        query = f''' INSERT INTO metadata (db_name, sender, send_to, unix_time)
              VALUES(?, ?, ?, ?)'''
        try: 
            self.cur.execute(query, message_info)
            self.conn.commit()
        except Exception as e:
            logging.error('MetaDataDB:add_emailed_data. db_name: {db_name}, sender: {sender}, send_to: {send_to}', exc_info=True)

    def close_connection(self):
        logging.info('MetaDataDB:close_connection. Closing db connection')
        try:
            self.cur.close()
            self.conn.close()
        except Exception as e:
            logging.error(f'MetaDataDB:close_connection. Error', exc_info=True)
        
import time
import logging

import traceback
import docx2txt
from PyQt5.QtCore import QThread, pyqtSlot, pyqtSignal

import configs
from emailer.helper import EmailHelper
from constants import email_template, storage_keys
from store import UserStore
from .meta_db import MetaDataDB


class SendEmailOutlook(QThread):
    user_store = UserStore()
    email_helper = EmailHelper()
    exception_signal = pyqtSignal(str, str)
    sent_count_signal = pyqtSignal(int, int)
    stop = False

    @pyqtSlot(bool)
    def stop_signal(self, stop):
        self.stop = stop

    def get_employees_list(self):
        employees = self.email_helper.get_employees_dict(self.active_msg_db_path)
        sent_emails_from_active_db = self.meta_db.get_active_db_sent_emails(self.active_msg_db_path)

        sender_emails_day_activity = []
        for sender_account in self.sender_accounts:
            sent_emails_count = self.meta_db.get_required_email_day_activity(self.active_msg_db_path,
                                                                             sender_account['email'])
            sender_emails_day_activity.append({'email': sender_account['email'], 'count': sent_emails_count})

        self.employees_info_list = self.email_helper.get_employees_sorted_by_sender_emails(
            employees,
            sender_emails_day_activity,
            sent_emails_from_active_db
        )

    def run(self):
        try:
            import win32com.client
            self.meta_db = MetaDataDB()
            outlook = win32com.client.Dispatch('outlook.application')
            accounts = win32com.client.Dispatch("Outlook.Application").Session.Accounts
            full_message = docx2txt.process(email_template)
            attachments = self.user_store.get(storage_keys.attachments_paths)
            self.active_msg_db_path = self.user_store.get(storage_keys.message_db_path)

            if 'Message:' in full_message:
                message_list = full_message.split('Message:')
            else:
                message_list = ['', full_message]
            message_list[0] = message_list[0].strip('\n')
            message_list[1] = message_list[1].strip('\n')
            subject = message_list[0]
            message = message_list[1]
            self.sender_accounts = configs.get_sender_accounts()
            self.get_employees_list()

            for account in accounts:
                account_name = account.DisplayName
                sender_emails_list = [list(sender_account.values())[0] for sender_account in self.sender_accounts]
                if account_name in sender_emails_list:
                    account_position = sender_emails_list.index(account_name)
                    employees_info = self.employees_info_list[account_position]
                    sender_account_obj = account
                else:
                    print('skip account:', account)
                    continue
                sent_count = 0
                for employee_info in employees_info:
                    customized_subject = subject.replace(
                        '{{first_name}}', employee_info['first_name']).replace(
                        '{{company}}', employee_info['company']).replace(
                        '{{email}}', employee_info['email'])
                    customized_message = message.replace(
                        '{{first_name}}', employee_info['first_name']).replace(
                        '{{company}}', employee_info['company']).replace(
                        '{{email}}', employee_info['email'])
                    mail = outlook.CreateItem(0)
                    mail._oleobj_.Invoke(*(64209, 0, 8, 0, sender_account_obj))
                    mail.To = employee_info['email']
                    mail.Subject = customized_subject
                    # mail.Body = 'Message body'
                    mail.HTMLBody = customized_message
                    for attachment in attachments:
                        mail.Attachments.Add(attachment)
                    mail.Send()
                    sent_count += 1
                    self.meta_db.add_emailed_data(
                        self.active_msg_db_path,
                        sender_account_obj.DisplayName,
                        employee_info['email']
                    )
                    print('Sender:', account_name, 'Sent:', employee_info['email'], ' | ', 'Sent emails number:',
                          sent_count)
                    self.sent_count_signal.emit(sent_count, account_position)
                    time.sleep(0.5)  # delay after evry sent email
                    if self.stop:
                        logging.info('SendEmailOutlook:run. Stoped manually')
                        return
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, ('There is technical problem.'
                                            ' Please stop and try later'))

import openpyxl
from constants import paths 
import logging

isImported = False
values_dict_list = []

def get_sender_accounts():
    global isImported
    if isImported:
        logging.info('configs:get_sender_accounts. already imported')
        return values_dict_list
    isImported = True
    book = openpyxl.load_workbook(paths.sender_accounts, data_only=True)
    sheets = book.sheetnames
    sheet = book[sheets[0]]
    rows = sheet.rows
    key_list = ['email', 'password', 'host_name']
    len_columns = len(key_list)
    for row in rows:
        values_dict = {}
        for i, cell in enumerate(row):
            if i >= len_columns:
                continue
            if cell.value:
                values_dict[key_list[i]] = cell.value
            else:
                logging.error('configs:get_sender_accounts. can not read accounts')
                values_dict[key_list[i]] = ''
                return []
        if values_dict in values_dict_list:
            logging.warn(f'configs:get_sender_accounts. pass duplicate account {values_dict}')
            continue

        values_dict_list.append(values_dict)
    book.close()
    return values_dict_list
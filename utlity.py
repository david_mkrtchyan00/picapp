from sys import platform
import os
import subprocess

def is_mac():
    return platform == "darwin"

def is_windows():
    return  platform == "win32"

def is_linux():
    return platform == "linux" or platform == "linux2"

def open_file(filename):
    if platform == "win32":
        os.startfile(filename)
    else:
        opener = "open" if platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])
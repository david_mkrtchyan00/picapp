import os
import logging

import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration
from dotenv import load_dotenv

from constants import paths

def init():
    load_dotenv()

    debug = (os.getenv('debug', 'False') == False)
    env = os.getenv('env') or 'production'

    if debug:
        log_file = None
    else:
        log_file = paths.app_log
    logging.basicConfig(filename=log_file, format='[%(asctime)s]:[%(levelname)s]: %(message)s', level=logging.INFO)
        
    # if test env not use sentry
    if env == 'test':
        return env, debug
    
    sentry_logging = LoggingIntegration(
        level=logging.INFO,        # Capture info and above as breadcrumbs
        event_level=logging.CRITICAL  # Send errors as events
    )

    sentry_sdk.init(
    dsn="https://902c8e97daf44e589b0d58e2398fd695@o4504929529036800.ingest.sentry.io/4504929531658240",
    environment=env,
    integrations=[
            sentry_logging,
        ],
    traces_sample_rate=1.0,
    )
    return env, debug
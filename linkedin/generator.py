import os
import openpyxl
import traceback
from PyQt5 import QtCore

from .getter import Getter


class EGenerator(QtCore.QThread):
    exception_signal=QtCore.pyqtSignal(str,str)
    finish_signal=QtCore.pyqtSignal(str)
    def run(self):
        try:
            self.generateEmails()
            self.finish_signal.emit('Emails generated successfully')
        except:
            tb=str(traceback.format_exc())
            self.exception_signal.emit(tb,('Can not generate emails.'
                                            'Please check database and email structure'))
        

    def read_company_employes_data(self):
        db_path1=f'database/stored by company/{self.company_name}.xlsx'
        db_path2=f'database/stored by company/{self.company_name} sales nav.xlsx'
        isOldData=os.path.isfile(db_path1)
        isOldDataPremium=os.path.isfile(db_path2)
        if isOldData:
            self.db_path=db_path1
        elif isOldDataPremium:
            self.db_path=db_path2
        else:
            return []
        book = openpyxl.load_workbook(self.db_path, data_only=True)
        sheets=book.sheetnames
        sheet = book[sheets[0]]
        rows = sheet.rows
        key_list=['first_name','last_name','profession','location']
        values_dict_list=[]
        for row in rows:
            values_dict={}
            for i,cell in enumerate(row):
                values_dict[key_list[i]]=cell.value
            values_dict_list.append(values_dict)
        book.close()
        return values_dict_list

    def generateEmails(self):
        getObj=Getter()
        getObj.get_company_info()
        self.company_name=getObj.company_info['company']
        self.oldCompanyEmployesData=self.read_company_employes_data()
        if not self.oldCompanyEmployesData:
            raise('Company file not found or not in form')
        # print(self.oldCompanyEmployesData)
        getObj.data_dicts_list=self.oldCompanyEmployesData
        if 'sales nav' in self.db_path:
            # print(self.db_path)
            getObj.isPremiumRequet=True
        else:
            getObj.isPremiumRequet=False
        getObj.saveCompanyEmployesData()
        getObj.create_emails()
        getObj.add_company_employes_emails()
        getObj.add_company_data_right_look()
        getObj.workbook.close()
        getObj.update_data_dicts_list()
        getObj.db_path=getObj.getDatabasesLocation()['database_location']
        old_database_dicts_list=getObj.read_old_database()
        getObj.updateDatabase(old_database_dicts_list)
# gen_obj=EGenerator()
# gen_obj.run()
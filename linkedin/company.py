import time
import random
import os
import glob
import re

from PyQt5 import QtCore
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import xlsxwriter
import traceback


class CompanyGetter(QtCore.QThread):
    back_signal_stop = QtCore.pyqtSignal()
    exception_signal = QtCore.pyqtSignal(str, str)
    company_data_dicts_list = list()
    stop_scraping = False

    @QtCore.pyqtSlot(str)
    def start_stop_signal(self, start_stop_scraping):
        if start_stop_scraping == 'start':
            self.start_scraping = True
        elif start_stop_scraping == 'stop':
            # self.stop_scraping()
            self.stop_scraping = True

    def run(self):
        try:
            self.open_linkedin()
            self.start_scraping = False
            # time.sleep(20)
            self.check_to_start_scraping()
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Chromedriver error. Please try again')
            return

        isNewpPage = True
        while isNewpPage and not self.stop_scraping:
            try:
                self.get_company_data()
            except:
                break  # maybe emit error(check)
            try:
                isNewpPage = self.change_page()
            except:
                break
        try:
            self.saveInDatabase()
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Can not save data')
            return
            # print(self.company_data_dicts_list)
        try:
            self.driver.quit()
        except:
            pass
        self.back_signal_stop.emit()

    def open_linkedin(self):
        options = webdriver.ChromeOptions()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        chromedriver = "./cache/chromedriver.exe"
        os.environ["webdriver.chrome.driver"] = chromedriver
        options.add_argument("user-data-dir=/tmp/tarun")
        self.driver = webdriver.Chrome(executable_path=chromedriver, options=options)
        self.driver.get('https://www.linkedin.com/sales?trk=d_sales2_nav_snlogo')

    def check_to_start_scraping(self):
        print('on check for start')
        while True:
            if self.start_scraping:
                return
            time.sleep(5)

    def get_company_data(self):
        self.main_page_link = self.driver.current_url
        for k in range(8):
            time.sleep(2)
            html = self.driver.page_source
            soup = BeautifulSoup(html, 'lxml')
            search_result = soup.find('section', {'id': 'results'}).find('ol',
                                                                         {'class': 'search-results__result-list'})
            whole_each_first_div = search_result.findAll('div',
                                                         {'class': 'search-results__result-container full-width'})
            self.get_attribute(whole_each_first_div)

            self.driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
            time.sleep(1)
        self.get_website()
        self.driver.get(self.main_page_link)
        time.sleep(5)
        self.driver.find_element_by_tag_name('body').send_keys(Keys.END)
        time.sleep(3)
        self.driver.find_element_by_tag_name('body').send_keys(Keys.END)

    def get_attribute(self, whole_each_first_div):
        for each_first_div in whole_each_first_div:
            name = each_first_div.find('dt', {'class': 'result-lockup__name'}).text
            name_href = each_first_div.find('a', {'class': 'ember-view'}, href=True)
            link = name_href.get('href')
            link = link.replace('?', '/people?')
            link = 'https://www.linkedin.com' + link
            if len(re.findall('google', link)) > 0 and len(re.findall('map', link)) > 0:
                link = ''
            emloyee_and_location = each_first_div.find('ul',
                                                       {'class': 'mv1 Sans-12px-black-60% result-lockup__misc-list'})
            try:
                employee = emloyee_and_location.findAll('li',
                                                        {'class': 'result-lockup__misc-item'})[1].text
            except:
                employee = ''
            try:
                location = emloyee_and_location.findAll('li',
                                                        {'class': 'result-lockup__misc-item'})[2].text
            except:
                location = ''
            data_dict = {}
            try:
                data_dict['Name'] = name.strip()
                data_dict['Employes'] = employee.strip()
                data_dict['Location'] = location.strip()
                data_dict['Link'] = link
                if data_dict not in self.company_data_dicts_list:
                    self.company_data_dicts_list.append(data_dict)
            except Exception as e:
                print(e, '   Can not get record data')

    def get_website(self):
        requested_list = []
        for dataDict in self.company_data_dicts_list:
            if dataDict['Link'] in requested_list:  # Why link will have dublicate?
                continue
            self.driver.get(dataDict['Link'])
            requested_list.append(dataDict['Link'])
            time.sleep(3)
            try:
                website_element = self.driver.find_element_by_xpath(
                    '//a[@class="inverse-link-on-a-light-background link-without-visited-state meta-link"]')
                website = website_element.get_attribute("href")
                dataDict['Website'] = website
            except:
                dataDict['Website'] = ''

    def change_page(self):
        time.sleep(1)
        self.driver.find_element_by_xpath('//button[@class="search-results__pagination-next-button"]').click()
        time.sleep(random.randint(5, 10))
        if self.main_page_link == self.driver.current_url:
            self.driver.close()
            return False
        return True

    def start_scraping(self):
        self.driver.close()

    def saveInDatabase(self):
        files = glob.glob("database/sales nav companies 00*.xlsx")
        files_count = str(len(files) + 1)
        workbook = xlsxwriter.Workbook('sales nav companies 00' + files_count + '.xlsx')
        worksheet = workbook.add_worksheet()
        for i, each_dict in enumerate(self.company_data_dicts_list):
            worksheet.write(i, 0, str(each_dict['Name']))
            worksheet.write(i, 1, str(each_dict['Employes']))
            worksheet.write(i, 2, str(each_dict['Location']))
            worksheet.write(i, 3, str(each_dict['Website']))
        workbook.close()

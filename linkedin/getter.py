import time
import random
import os
import re
import ast
import logging

from PyQt5 import QtCore
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import xlsxwriter
import openpyxl
import traceback
from unidecode import unidecode
from selenium.webdriver.common.by import By
from constants import storage_keys, chromedriver_file, paths
from store import UserStore
from utlity import open_file


class Getter(QtCore.QThread):
    data_dicts_list = []
    start_scraping = False
    stop_scraping = False
    user_store = UserStore()
    back_signal_stop = QtCore.pyqtSignal(str, bool)
    exception_signal = QtCore.pyqtSignal(str, str)

    @QtCore.pyqtSlot(bool, bool)
    def get_linkedin_signal(self, useCompanyInfo, isPremiumRequet):
        self.useCompanyInfo = useCompanyInfo
        self.isPremiumRequet = isPremiumRequet

    @QtCore.pyqtSlot(str)
    def start_stop_signal(self, start_stop_scraping):
        if start_stop_scraping == 'start':
            self.start_scraping = True
        elif start_stop_scraping == 'stop':
            # self.finish_scraping()
            self.stop_scraping = True

    def run(self):
        self.data_dicts_list = []
        try:
            self.open_linkedin()
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Chromedriver error. Please try again')
            return
        try:
            if self.useCompanyInfo:
                self.get_company_info()
            else:
                self.get_company_name()
            self.get_not_allowed_words_list()
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Provided data not correct')
            return
        try:
            self.extract_data()
        except Exception as e:
            # print('Can not find new data')
            print(e)
            # tb=str(traceback.format_exc())
            # self.exception_signal.emit(tb, 'Can not extract linkedin data')
            # return 
        try:
            self.db_path = self.user_store.get(storage_keys.db_path)
            self.saveCompanyEmployesData()
            if self.useCompanyInfo:
                self.create_emails()
                self.add_company_employes_emails()
                self.add_company_data_right_look()
            self.workbook.close()
            if self.useCompanyInfo:
                self.update_data_dicts_list()
                old_database_dicts_list = self.read_old_database()
                self.updateDatabase(old_database_dicts_list)
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Can not save data')
            return

        try:
            self.driver.quit()
        except:
            pass
        self.back_signal_stop.emit('finish', self.isPremiumRequet)

    def open_linkedin(self):
        logging.info('Getter::open_linkedin: start')
        self.new_page_data = True
        options = webdriver.ChromeOptions()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        # chromedriver=dir_path+'/chromedriver'#/EXE/dist
        chromedriver = chromedriver_file
        os.environ["webdriver.chrome.driver"] = chromedriver
        options.add_argument("user-data-dir=/tmp/tarun")
        self.driver = webdriver.Chrome(executable_path=chromedriver, options=options)
        if self.isPremiumRequet:
            self.driver.get('https://www.linkedin.com/sales?trk=d_sales2_nav_snlogo')
        else:
            self.driver.get('https://www.linkedin.com')
        if self.useCompanyInfo:
            if os.path.isfile(paths.company_info):
                open_file(paths.company_info)
            else:
                raise ('info file not found')
        else:
            if os.path.isfile(paths.company_name):
                open_file(paths.company_name)
            else:
                raise ('company_name file not found')
        # input('press enter:')
        #### signal fix time sleep to 5 second
        self.check_to_start_scraping()
        # time.sleep(20)

    def check_to_start_scraping(self):
        while True:
            if self.start_scraping:
                return
            time.sleep(5)

    def get_company_name(self):
        with open(paths.company_name, "r") as file:
            info_text = file.read()
        self.company_info = ast.literal_eval(info_text)

    def get_company_info(self):
        with open(paths.company_info, "r") as file:
            info_text = file.read()
        self.company_info = ast.literal_eval(info_text)

    def get_not_allowed_words_list(self):
        not_allowed_profession_list = ['ARCH', 'ARCHI', 'ARCHITECTE',
                                       'ARCHITECTES', 'ARCHITECTURE', 'ARCHITECTURES']
        self.not_allowed_profession_list = self.generate_not_allowed_word_variant(
            not_allowed_profession_list)
        with open(paths.not_allowed_names, 'r') as file:
            not_allowed_names_list = ast.literal_eval(file.read())
        self.not_allowed_names_list = self.generate_not_allowed_word_variant(
            not_allowed_names_list)

    def generate_not_allowed_word_variant(self, not_allowed_words_list):
        for i in range(len(not_allowed_words_list)):
            not_allowed_words_list.append(not_allowed_words_list[i].capitalize())
            not_allowed_words_list.append(not_allowed_words_list[i].lower())
        return not_allowed_words_list

    def extract_data(self):
        page_counter = 1
        while self.new_page_data and not self.stop_scraping:
            if self.isPremiumRequet:
                next_button_xpath = '//button[@class="artdeco-pagination__button artdeco-pagination__button--next artdeco-button artdeco-button--muted artdeco-button--icon-right artdeco-button--1 artdeco-button--tertiary ember-view"]'
                page_len = 10
                self.driver.find_element(By.TAG_NAME, 'body').click()
            else:
                page_len = 4
                next_button_xpath = '//button[@class="artdeco-pagination__button artdeco-pagination__button--next artdeco-button artdeco-button--muted artdeco-button--icon-right artdeco-button--1 artdeco-button--tertiary ember-view"]'

            for k in range(page_len):
                time.sleep(1)
                self.get_attributs()
                self.driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.PAGE_DOWN)
            if page_counter < 2:
                time.sleep(1)
                current_page = self.driver.current_url
                self.driver.find_element(By.XPATH, next_button_xpath).click()
                time.sleep(random.randint(7, 12))
                next_page = self.driver.current_url
                if current_page == next_page:
                    self.new_page_data = False
                    print('Second page not found ')
                    return
            else:
                current_page = self.driver.current_url
                next_page_url = current_page.replace('page=' + str(page_counter),
                                                     'page=' + str(int(page_counter) + 1))
                self.driver.get(next_page_url)
                time.sleep(random.randint(2, 6))
            page_counter += 1

    def get_attributs(self):
        try:
            time.sleep(1)
            html = self.driver.page_source
            soup = BeautifulSoup(html, 'lxml')
            if self.isPremiumRequet:
                search_result = soup.find('div', {'id': 'search-results-container'}).find('ol',
                                                                                          {
                                                                                              'class': 'artdeco-list background-color-white _border-search-results_1igybl'})
                each_record_xpath = 'artdeco-list__item pl3 pv3'

                each_results_list = search_result.findAll('li', {'class': each_record_xpath})
            else:
                # search_result_xpath='reusable-search__entity-results-list list-style-none'
                # search_result=soup.find('ul',{'class':search_result_xpath})   

                # search_div='pv2 artdeco-card ph0 mb2'
                # search_result=soup.find('div',{'class':search_div})  

                each_record_xpath = 'entity-result'
                each_results_list = soup.findAll('div', {'class': each_record_xpath})
            if not each_results_list:
                print('No result')
                return

            if len(each_results_list) < 2:
                self.new_page_data = False
                print("Contacts less than 2\nnew page not found\nbreaking")
        except Exception as e:
            print(e)

        for i, each_result in enumerate(each_results_list):
            try:
                if self.isPremiumRequet:
                    full_name_spaces = each_result.find('span', {'data-anonymize': 'person-name'})
                else:
                    full_name_spaces = each_result.find('span',
                                                        {'dir': 'ltr'}).find('span')  # 'name actor-name'
                full_name_spaces = full_name_spaces.text
                full_name_clear = full_name_spaces.strip()
            except Exception as e:
                # print(e)
                full_name_clear = ''
            try:
                if self.isPremiumRequet:
                    profession = each_result.find('span',
                                                  {'data-anonymize': 'title'})
                else:
                    profession = each_result.find('div',
                                                  {'class': 'entity-result__primary-subtitle t-14 t-black t-normal'})
                    # 'subline-level-1 t-14 t-black t-normal search-result__truncate'
                profession = profession.text
                profession_clear = profession.strip()
            except:
                profession_clear = ''
            try:
                if self.isPremiumRequet:
                    location = each_result.find('span', {'data-anonymize': 'location'})
                else:
                    location = each_result.find('div',
                                                {'class': 'entity-result__secondary-subtitle t-14 t-normal'})
                    # ('subline-level-2 t-12 t-black--light t-normal '
                    # 'search-result__truncate')
                location = location.text
                location_clear = location.strip()
            except:
                location_clear = ''
            if full_name_clear == '' and profession_clear == '' and location_clear == '':
                # print('skip 1 continue')
                continue
            self.clean_data(full_name_clear, profession_clear, location_clear)

    def clean_data(self, full_name_clear, profession_clear, location_clear):
        try:
            full_name_clear = unidecode(full_name_clear)
            full_name_clear = full_name_clear.replace("'", "").replace("`", "")
            full_name_clear = " ".join(re.findall("[a-zA-Z-]+",
                                                  full_name_clear)).replace("  ", " ")
            for not_allowed_name in self.not_allowed_names_list:
                fist_part_not_allowed_name = ' ' + not_allowed_name
                final_part_not_allowed_name = not_allowed_name + ' '
                if not_allowed_name in full_name_clear:
                    if (fist_part_not_allowed_name in full_name_clear or
                            final_part_not_allowed_name in full_name_clear):
                        full_name_clear.replace(not_allowed_name, '')
            full_name_list = full_name_clear.split()
            for name in full_name_list:
                if len(name) < 2:
                    full_name_list.remove(name)
            if len(full_name_list) == 3 and len(full_name_list[1]) < 3:
                first_name = full_name_list[0]
                last_name = full_name_list[2]
            elif len(full_name_list) > 3:
                first_name = full_name_list[0]
                last_name = full_name_list[1]
            # elif len(full_name_list)==2 and len(full_name_list[1])<3:
            #     continue
            #     print("Skiping",full_name_list)

            # elif len(full_name_list)==4 and len(full_name_list[1])<3:
            #     first_name=full_name_list[0]
            #     last_name=full_name_list[2]
            elif len(full_name_list) == 2:
                first_name = full_name_list[0]
                last_name = full_name_list[1]
            else:
                try:
                    first_name = full_name_list[0]
                    last_name = full_name_list[1]
                except:
                    first_name = ''
                    last_name = ''
        except Exception as e:
            print(e)
            first_name = ''
            last_name = ''
        self.create_data_list(first_name, last_name, profession_clear, location_clear)

    def create_data_list(self, first_name, last_name, profession_clear, location_clear):
        data_dict = {}
        first_name = unidecode(first_name)
        last_name = unidecode(last_name)
        first_name = first_name.replace("'", "").replace("`", "")
        last_name = last_name.replace("'", "").replace("`", "")
        first_name = " ".join(re.findall("[a-zA-Z-]+", first_name)).replace(" ", "")
        last_name = " ".join(re.findall("[a-zA-Z-]+", last_name)).replace(" ", "")
        if (len(first_name) < 2 or len(last_name) < 2 or
                first_name in self.not_allowed_profession_list or
                last_name in self.not_allowed_profession_list):
            # print("Skiping ",first_name,last_name)
            return
            ####
        data_dict['first_name'] = first_name.capitalize()
        data_dict['last_name'] = last_name.capitalize()
        data_dict['profession'] = profession_clear
        data_dict['location'] = location_clear
        # print(data_dict)
        if data_dict not in self.data_dicts_list:
            self.data_dicts_list.append(data_dict)

    def finish_scraping(self):
        self.db_path = self.user_store.get(storage_keys.db_path)

        self.saveCompanyEmployesData()
        if self.useCompanyInfo:
            self.create_emails()
            self.add_company_employes_emails()
            self.add_company_data_right_look()
        self.workbook.close()
        if self.useCompanyInfo:
            self.update_data_dicts_list()
            old_database_dicts_list = self.read_old_database()
            self.updateDatabase(old_database_dicts_list)
        self.driver.quit()
        # self.back_signal_stop.emit('finish',self.isPremiumRequet)
        return

    def saveCompanyEmployesData(self):
        provided_company = " ".join(re.findall("[a-zA-Z]+", self.company_info['company']))
        if self.isPremiumRequet:
            provided_company += ' sales nav'
        self.workbook = xlsxwriter.Workbook(
            'database/stored by company/' + provided_company + '.xlsx')
        worksheet = self.workbook.add_worksheet()
        for i, each_dict in enumerate(self.data_dicts_list):
            worksheet.write(i, 0, str(each_dict['first_name']))
            worksheet.write(i, 1, str(each_dict['last_name']))
            worksheet.write(i, 2, str(each_dict['profession']))
            worksheet.write(i, 3, str(each_dict['location']))

    def create_emails(self):
        self.email_list = []
        info = self.company_info
        for each_dict in self.data_dicts_list:
            first_name = each_dict['first_name']
            last_name = each_dict['last_name']
            if len(info['email']['first_part']) == 4:
                if info['email']['first_part'].isupper():
                    first_part = first_name.upper()
                elif info['email']['first_part'].islower():
                    first_part = first_name.lower()
                else:
                    first_part = first_name
            elif len(info['email']['first_part']) == 1:
                if info['email']['first_part'] == 'J':
                    first_part = first_name[0].upper()
                elif info['email']['first_part'] == 'j':
                    first_part = first_name[0].lower()
                elif info['email']['first_part'] == 'n':
                    first_part = first_name[-1].lower()
                elif info['email']['first_part'] == 'N':
                    first_part = first_name[-1].upper()
                elif info['email']['first_part'] == 'S':
                    first_part = last_name[0].upper()
                elif info['email']['first_part'] == 's':
                    first_part = last_name[0].lower()
                elif info['email']['first_part'] == 'h':
                    first_part = last_name[-1].lower()
                elif info['email']['first_part'] == 'H':
                    first_part = last_name[-1].upper()
            elif len(info['email']['first_part']) == 2:
                if info['email']['first_part'].isupper():
                    first_part = first_name[:2].upper()
                elif info['email']['first_part'].islower():
                    first_part = first_name[:2].lower()
                else:
                    first_part = first_name[:2]
            elif len(info['email']['first_part']) == 5:
                if info['email']['first_part'].isupper():
                    first_part = last_name.upper()
                elif info['email']['first_part'].islower():
                    first_part = last_name.lower()
                else:
                    first_part = last_name
            else:
                first_part = ''
            if len(info['email']['second_part']) == 5:
                if info['email']['second_part'].isupper():
                    second_part = last_name.upper()
                elif info['email']['second_part'].islower():
                    second_part = last_name.lower()
                else:
                    second_part = last_name
            elif len(info['email']['second_part']) == 1:
                if info['email']['second_part'] == 'S':
                    second_part = last_name[0].upper()
                elif info['email']['second_part'] == 's':
                    second_part = last_name[0].lower()
                elif info['email']['second_part'] == 'h':
                    second_part = last_name[-1].lower()
                elif info['email']['second_part'] == 'H':
                    second_part = last_name[-1].upper()
                elif info['email']['second_part'] == 'J':
                    second_part = first_name[0].upper()
                elif info['email']['second_part'] == 'j':
                    second_part = first_name[0].lower()
                elif info['email']['second_part'] == 'n':
                    second_part = first_name[-1].lower()
                elif info['email']['second_part'] == 'N':
                    second_part = first_name[-1].upper()
            elif len(info['email']['second_part']) == 2:
                if info['email']['second_part'].isupper():
                    second_part = last_name[:2].upper()
                elif info['email']['second_part'].islower():
                    second_part = last_name[:2].lower()
                else:
                    second_part = last_name[:2]
            elif len(info['email']['second_part']) == 4:
                if info['email']['second_part'].isupper():
                    second_part = first_name.upper()
                elif info['email']['second_part'].islower():
                    second_part = first_name.lower()
                else:
                    second_part = first_name
            else:
                second_part = ''
            if first_part or second_part:
                first_part = " ".join(re.findall("[a-zA-Z]+",
                                                 first_part)).replace(" ", "")
                second_part = " ".join(re.findall("[a-zA-Z]+",
                                                  second_part)).replace(" ", "")
                email = (first_part + info['email']['delimiter'] +
                         second_part + info['email']['end_of_format'])
            else:
                email = ''
            self.email_list.append(email)

    def add_company_employes_emails(self):
        # Add sheet 2 to store employees first name, last name and email 
        worksheet1 = self.workbook.add_worksheet()
        for i, each_dict in enumerate(self.data_dicts_list):
            worksheet1.write(i, 0, str(each_dict['first_name']))
            worksheet1.write(i, 1, str(each_dict['last_name']))
            worksheet1.write(i, 2, str(self.email_list[i]))

    def add_company_data_right_look(self):
        # Add sheet 3 to store final data. Employees first name, company and email
        worksheet2 = self.workbook.add_worksheet()
        for i, each_dict in enumerate(self.data_dicts_list):
            worksheet2.write(i, 0, str(each_dict['first_name']))
            worksheet2.write(i, 1, self.company_info['company'])
            worksheet2.write(i, 2, str(self.email_list[i]))

    def update_data_dicts_list(self):
        for i, each_dict in enumerate(self.data_dicts_list):
            each_dict['company'] = self.company_info['company']
            each_dict['email'] = self.email_list[i]

    def read_old_database(self):
        # self.db_path='database/Database in use.xlsx'
        isOldData = os.path.isfile(self.db_path)
        if not isOldData:
            return []
        book = openpyxl.load_workbook(self.db_path, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = sheet.rows
        key_list = ['first_name', 'company', 'email', 'feedback']
        len_columns = len(key_list)
        values_dict_list = []
        for row in rows:
            values_dict = {}
            for i, cell in enumerate(row):
                if i >= len_columns:
                    continue
                if cell.value:

                    values_dict[key_list[i]] = cell.value
                else:
                    values_dict[key_list[i]] = ''
            color = row[0].fill.start_color.index
            if str(color) != '00000000' and len(str(color)) == 8:
                values_dict['color'] = '#' + str(color)[2:]
            else:
                values_dict['color'] = ''
            values_dict_list.append(values_dict)
        book.close()
        return values_dict_list

    def updateDatabase(self, old_database_dicts_list, sent=False):
        database_dicts_list = old_database_dicts_list + self.data_dicts_list
        # os.remove(self.db_path)
        self.workbook = xlsxwriter.Workbook(self.db_path)
        worksheet1 = self.workbook.add_worksheet('Sheet1')
        i = 0
        database_dicts_list = sorted(database_dicts_list,
                                     key=lambda k: k['email'])
        for each_dict in database_dicts_list:
            if not each_dict['email']:
                continue
            if 'feedback' in each_dict.keys() and each_dict['feedback'] == 'Undelivered':
                self.format1 = self.workbook.add_format(
                    {'bg_color': '#FFC7CE'})  # ,'font_color': '#9C0006'
            elif 'feedback' in each_dict.keys() and each_dict['feedback'] == 'Delivered':
                self.format1 = self.workbook.add_format(
                    {'bg_color': '#8cfb8c'})
            else:
                self.format1 = False
            if 'color' in each_dict.keys() and each_dict['color']:
                self.format1 = self.workbook.add_format(
                    {'bg_color': each_dict['color']})
            worksheet1.write(i, 0, each_dict['first_name'], self.format1)
            worksheet1.write(i, 1, each_dict['company'], self.format1)
            worksheet1.write(i, 2, each_dict['email'], self.format1)
            i += 1
            # if 'feedback_status' in each_dict.keys():
            #     worksheet1.write(i, 4, each_dict['feedback_status'],self.format1)
            # #'feedback' in each_dict.keys()
            # #?? isFeedback=True, xi pti amen angam stugvi??
            # if self.format1:
            #     worksheet1.write(i, 3, 'Undelivered',self.format1)
        return self.mailing_database(database_dicts_list, sent)

    def mailing_database(self, database_dicts_list, sent):
        sorted_database_dicts_list = sorted(database_dicts_list,
                                            key=lambda k: k['email'])
        worksheet2 = self.workbook.add_worksheet('Mailing list')

        format45 = False
        if sent:
            format4 = self.workbook.add_format({'bg_color': '#17EEF1'})
            format5 = self.workbook.add_format({'bg_color': '#3EF539'})
            format45 = True
            sent_times = sent // 495  # 495
        i = 0
        for each_dict in sorted_database_dicts_list:
            if not each_dict['email']:
                continue
            if sent and sent > i:
                if (i // 495) % 2 == 0:  # 495
                    format45 = format4
                else:
                    format45 = format5
            else:
                format45 = False
            worksheet2.write(i, 0, str(each_dict['first_name']), format45)
            worksheet2.write(i, 1, each_dict['company'], format45)
            worksheet2.write(i, 2, each_dict['email'], format45)
            i += 1
        self.workbook.close()

# getObj=Getter()
# getObj.run()

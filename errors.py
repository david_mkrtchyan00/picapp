import json
import os
from PyQt5.QtWidgets import QLabel, QDialog, QDialogButtonBox, QFormLayout
from constants import errors_file

class SaveError(object):
    @staticmethod
    def save_error_msg(error_mesage):
        if os.path.isfile(errors_file):
            with open(errors_file, 'r') as file:
                errors_list=json.load(file)
        else:
            errors_list=[]
        errors_list.append(str(error_mesage))
        with open(errors_file, 'w') as file:
            json.dump(errors_list, file, indent=1)
        print('Error saved in json')


class ErrorDialog(QDialog):
    def __init__(self, parent=None, error_message='Something is wrong. Error message not found'):
        super().__init__(parent)
        self.setWindowTitle('Error message')
        # self.sample_password = QLineEdit(self)
        # self.sample_password .setEchoMode(QLineEdit.Password)
        error_message_label=QLabel('\n'+error_message+'\n', self)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok, self)
        self.layout = QFormLayout(self)
        self.layout.addRow(error_message_label)
        self.layout.addWidget(buttonBox)
        buttonBox.accepted.connect(self.accept)
        # buttonBox.rejected.connect(self.reject)
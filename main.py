# Email marketing tool for Pictury 

__author__ = 'Davit Mkrtchyan'
__email__ = 'david.mkrtchyan00@gmail.com'

from setup import migrate

migrate.setup()

import sys
import os
import time
import traceback
import logging

import multiprocessing as mp
from PyQt5.QtGui import QPainter, QPen, QColor, QFont
from PyQt5.QtWidgets import (QWidget, QApplication, QLabel,
                             QDialog, QMessageBox, QLineEdit,
                             QCheckBox, QPushButton, QFileDialog,
                             QRadioButton, QVBoxLayout, QScrollArea)
from PyQt5.QtCore import Qt, QThread, pyqtSlot, QRect, pyqtSignal

from extractor import ShowReview, ProgressBar, FeedbackExtractorSMTP, FeedbackExtractorOutlook
from linkedin.getter import Getter
from linkedin.company import CompanyGetter
from linkedin.generator import EGenerator
from emailer import SendEmailSMTP, SendEmailOutlook, UpdateMailingList, MetaDataDB
from errors import SaveError, ErrorDialog
import configs
from store import UserStore
from constants import storage_keys, email_service_outlook, email_service_smtp, paths
from utlity import is_mac, is_windows, open_file
from settings import init
from constants import email_template

env, debug = init()


class Main(QWidget):
    linkedin_signal = pyqtSignal(bool, bool)
    start_stop_signal = pyqtSignal(str)
    company_start_stop_signal = pyqtSignal(str)
    password_signal = pyqtSignal(str)
    email_formats_signal = pyqtSignal(list)
    feedback_path_signal = pyqtSignal(str)
    email_signal = pyqtSignal(dict)
    email_stop_signal = pyqtSignal(bool)
    email_signal_finder = pyqtSignal(list)
    terminate_feedback_extractor = pyqtSignal()
    user_store = UserStore()
    email_details = MetaDataDB()

    def __init__(self):
        super().__init__()
        self.sent_emails_count = -1
        self.email_sender_labels = []
        self.initUI()

    def initUI(self):
        # ------------main configs------------
        self.setGeometry(50, 50, 1200, 670)
        self.setFixedSize(1200, 670)
        self.setWindowTitle('PicApp')
        background_color = QColor(40, 41, 35)
        p = self.palette()
        p.setColor(self.backgroundRole(), background_color)
        self.setPalette(p)
        top_info_labels_font = QFont()
        top_info_labels_font.setPointSize(15)
        self.info_labels_font = QFont()
        self.info_labels_font.setPointSize(11)

        # ----------Linkedin Getter------------

        label = QLabel("Linkedin Getter", self)
        label.move(200, 10)
        label.setFont(top_info_labels_font)
        label.setStyleSheet('color:white')
        label = QLabel("Feedback Getter", self)
        label.move(800, 10)
        label.setFont(top_info_labels_font)
        label.setStyleSheet('color:white')
        label = QLabel("Database Management", self)
        label.move(200, 360)
        label.setFont(top_info_labels_font)
        label.setStyleSheet('color:white')
        label = QLabel("Email Management", self)
        label.move(800, 360)
        label.setFont(top_info_labels_font)
        label.setStyleSheet('color:white')

        self.btn_mode = 1
        self.ln_btn = QPushButton(self, text="Linkedin Getter")
        self.ln_btn.setGeometry(QRect(20, 60, 250, 40))
        font = QFont()
        font.setPointSize(12)
        self.ln_btn.setFont(font)
        self.picapp_standart_btn_style = ("QPushButton::hover"
                                          "{"
                                          "background-color : #51a9f0;"
                                          "}"
                                          "QPushButton::pressed"
                                          "{"
                                          "background-color : #1068b0;"
                                          "}"
                                          "QPushButton"
                                          "{"
                                          "color: white;"
                                          "border-radius: 8px;"
                                          "background-color: #2196f5;"
                                          "selection-color: black;"
                                          "selection-background-color: black;"
                                          "}"
                                          )
        self.linkedin_btn_style = ("color: white;"
                                   "border-radius: 8px;"
                                   "background-color: #2196f5;"
                                   "selection-color: black;"
                                   "selection-background-color: black;"
                                   )
        self.ln_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.ln_btn.clicked.connect(self.linkedinGetter)

        self.premium_btn_mode = 1
        self.ln_premium_btn = QPushButton(self, text="Linkedin Getter Premium")
        self.ln_premium_btn.setGeometry(QRect(320, 60, 250, 40))
        self.ln_premium_btn.setFont(font)
        self.ln_premium_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.ln_premium_btn.clicked.connect(self.linkedinGetterPremium)
        self.company_btn_mode = 1
        self.ln_company_btn = QPushButton(self, text="Linkedin Getter Company")
        self.ln_company_btn.setGeometry(QRect(20, 150, 250, 40))
        self.ln_company_btn.setFont(font)
        self.ln_company_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.ln_company_btn.clicked.connect(self.linkedinGetterCompany)

        self.gen_emails_btn = QPushButton(self, text="Check Email Format")
        self.gen_emails_btn.setGeometry(QRect(320, 150, 250, 20))
        self.gen_emails_btn.setFont(font)
        self.picapp_second_standart_btn_style = ("QPushButton::hover"
                                                 "{"
                                                 "background-color : #f5b95b;"
                                                 "}"
                                                 "QPushButton::pressed"
                                                 "{"
                                                 "background-color : #c97b00;"
                                                 "}"
                                                 "QPushButton"
                                                 "{"
                                                 "color: white;"
                                                 "border-radius: 8px;"
                                                 "background-color: #EDA737;"
                                                 "selection-color: black;"
                                                 "selection-background-color: black;"
                                                 "}")
        self.gen_emails_btn.setStyleSheet(self.picapp_second_standart_btn_style)
        self.gen_emails_btn.clicked.connect(self.check_email_format)

        self.gen_emails_btn = QPushButton(self, text="Generate Emails")
        self.gen_emails_btn.setGeometry(QRect(320, 180, 250, 20))
        self.gen_emails_btn.setFont(font)
        self.gen_emails_btn.setStyleSheet(self.picapp_second_standart_btn_style)
        self.gen_emails_btn.clicked.connect(self.generate_emails)

        label = QLabel("Generate emails after scrap data using provided email format", self)
        label.move(20, 262)
        label.setFont(self.info_labels_font)
        label.setStyleSheet('color:white')

        self.b = QCheckBox("  ", self)
        will_generate_emails = self.user_store.get(storage_keys.generate_emails_checkbox)
        self.b.setChecked(will_generate_emails)
        self.useCompanyInfo = will_generate_emails
        self.b.stateChanged.connect(self.change_is_auto_generate_emails)
        self.b.setFont(self.info_labels_font)
        self.b.move(435, 252)
        self.b.resize(320, 40)

        self.db_labels_list = QLabel(f"{200 * ' '}", self)
        self.db_labels_list.move(20, 390)
        self.db_labels_list.setFont(self.info_labels_font)
        self.db_labels_list.setStyleSheet('color:white')

        main_db_location = self.user_store.get(storage_keys.db_path)
        db_name = os.path.basename(main_db_location)
        if (db_name):
            self.db_labels_list.setText(f'Selected database: {db_name} {" " * 100}')
            self.db_labels_list.setStyleSheet('color:green')

        self.choose_db = QPushButton(self, text="Choose database")
        self.choose_db.setGeometry(QRect(20, 600, 250, 40))
        self.choose_db.setFont(font)
        self.choose_db.setStyleSheet(self.picapp_standart_btn_style)
        self.choose_db.clicked.connect(self.choose_database)

        # -----------Extract Email----------------

        self.extract_feedback_btn = QPushButton(self, text="Extract feedback")
        self.extract_feedback_btn.setGeometry(QRect(620, 60, 250, 40))
        self.extract_feedback_btn.setFont(font)
        self.extract_feedback_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.extract_feedback_btn.clicked.connect(self.extract_feedback)

        self.extract_from_btn = QPushButton(self, text="Extract from")
        self.extract_from_btn.setGeometry(QRect(770, 130, 250, 40))
        self.extract_from_btn.setFont(font)
        self.extract_from_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.extract_from_btn.clicked.connect(self.extract_from)

        self.show_in_db = QPushButton(self, text="Show feedback in database")
        self.show_in_db.setGeometry(QRect(920, 60, 250, 40))
        self.show_in_db.setFont(font)
        self.show_in_db.setStyleSheet(self.picapp_standart_btn_style)
        self.show_in_db.clicked.connect(self.show_feedback_in_db)

        self.radiobuttonSMTP = QRadioButton("Send email using SMTP", self)
        self.radiobuttonSMTP.setStyleSheet('color:white')
        self.radiobuttonSMTP.move(620, 300)
        self.radiobuttonSMTP.toggled.connect(self.switch_email_service)

        self.radiobuttonOutlook = QRadioButton("Send emails using Outlook", self)
        if is_mac():
            self.radiobuttonOutlook.setDisabled(True)
        self.radiobuttonOutlook.setStyleSheet('color:white')
        self.radiobuttonOutlook.move(620, 320)
        self.radiobuttonOutlook.toggled.connect(self.switch_email_service)

        # --------------Email management--------------

        msg_db_location = self.user_store.get(storage_keys.message_db_path)
        self.msg_db_location_label = QLineEdit(self)
        self.msg_db_location_label.move(620, 390)
        self.msg_db_location_label.resize(450, 20)
        self.msg_db_location_label.setText(msg_db_location)

        self.browse_message_database = QPushButton(self, text="Browse")
        self.browse_message_database.setGeometry(QRect(1075, 390, 100, 20))
        self.browse_message_database.clicked.connect(self.change_message_database_path)

        self.sent_email_count_label = QLabel(f"Loading...{' ' * 100}", self)
        self.day_sent_email_count_label = QLabel(f"Loading...{' ' * 100}", self)

        self.active_msg_db_name = self.user_store.get(storage_keys.message_db_path)

        self.email_details.connect_msg_db()

        self.scroll_widget = QWidget()
        self.scroll_layout = QVBoxLayout(self.scroll_widget)

        scroll_area = QScrollArea(self)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(self.scroll_widget)
        scroll_area.setGeometry(620, 415, 280, 230)

        self.setWindowTitle("Accounts")

        self.email_sender_accounts = configs.get_sender_accounts()

        self.show_sent_emails_ui()

        self.day_sent_email_count_label.move(910, 430)
        self.day_sent_email_count_label.setFont(self.info_labels_font)
        self.day_sent_email_count_label.setStyleSheet('color:white')
        self.sent_email_count_label.move(910, 410)
        self.sent_email_count_label.setFont(self.info_labels_font)
        self.sent_email_count_label.setStyleSheet('color:white')

        self.send_email_btn_style = self.picapp_standart_btn_style
        self.send_email_btn_text = "Send emails"
        self.send_email_btn = QPushButton(self, text=self.send_email_btn_text)
        self.send_email_btn.setGeometry(QRect(920, 460, 250, 40))
        self.send_email_btn.setFont(font)
        self.send_email_btn.setStyleSheet(self.send_email_btn_style)
        self.send_email_btn.clicked.connect(self.start_sending_email)

        self.customize_email_btn = QPushButton(self, text="Customize email")
        self.customize_email_btn.setGeometry(QRect(920, 550, 250, 30))
        self.customize_email_btn.setFont(font)
        self.customize_email_btn.setStyleSheet(self.picapp_standart_btn_style)
        self.customize_email_btn.clicked.connect(self.customize_email)

        self.choose_attachment = QPushButton(self, text="Choose attachments")
        self.choose_attachment.setGeometry(QRect(920, 590, 250, 30))
        self.choose_attachment.setFont(font)
        self.choose_attachment.setStyleSheet(self.picapp_standart_btn_style)
        self.choose_attachment.clicked.connect(self.attach_file)

        self.accounts = QPushButton(self, text="My accounts")
        self.accounts.setGeometry(QRect(920, 510, 250, 30))
        self.accounts.setFont(font)
        self.accounts.setStyleSheet(self.picapp_standart_btn_style)
        self.accounts.clicked.connect(self.edit_my_accounts)

        attached_file = self.user_store.get(storage_keys.attachments_paths)
        attached_file_count = len(attached_file)
        if not attached_file:
            attached_file = 100 * ' '
        elif attached_file_count > 1:
            attached_file = f'Attached: {attached_file_count} files'
        else:
            attached_file = f'Attached: {attached_file[0]}'
        self.attached_file_label = QLabel(f"{attached_file} {100 * ' '}", self)
        self.attached_file_label.move(620, 650)
        self.attached_file_label.setFont(self.info_labels_font)
        self.attached_file_label.setStyleSheet('color:white')

        # -------- create threads ----------

        self.getter = Getter()
        self.extract_outlook_feedback = ExtractFeedback()
        self.linkedin_generator = EGenerator()
        self.init_email_management()

        self.update_mailing_list = UpdateMailingList()
        self.show_review = ShowReview()
        self.getter.exception_signal.connect(self.threadExeption)
        self.linkedin_generator.exception_signal.connect(self.threadExeption)
        self.linkedin_generator.finish_signal.connect(self.linkedin_generator_slot)
        self.feedback_path_signal.connect(self.show_review.path_signal)
        self.extract_outlook_feedback.exception_signal.connect(self.threadExeption)
        self.show_review.exception_signal.connect(self.threadExeption)
        self.update_mailing_list.exception_signal.connect(self.threadExeption)

    # ------ END UI ----------

    @pyqtSlot(str, bool)
    def linkedin_finish(self, finish_signal, isPremiumRequet):
        if isPremiumRequet:
            self.linkedin_getter.terminate()
            self.premium_btn_mode = 1
            self.ln_premium_btn.setText('Linkedin Getter Premium')
            self.ln_premium_btn.setStyleSheet(self.linkedin_btn_style)
        else:
            self.linkedin_getter.terminate()
            self.btn_mode = 1
            self.ln_btn.setText('Linkedin Getter')
            self.ln_btn.setStyleSheet(self.linkedin_btn_style)
        # if not self.database_management_details.isRunning():
        #     self.database_management_details.start() 

    @pyqtSlot()
    def linkedin_company_finish(self):
        self.ln_company_btn.setText('Linkedin Getter Company')
        self.ln_company_btn.setStyleSheet(self.linkedin_btn_style)
        self.linkedin_comapany_getter.terminate()

    @pyqtSlot(str)
    def linkedin_generator_slot(self, message):
        # if not self.database_management_details.isRunning():
        #     self.database_management_details.start()
        pop = ErrorDialog(self, message)
        pop.show()

    def linkedinGetter(self):
        isPremiumRequet = False
        self.btn_mode += 1
        if self.btn_mode == 5:
            self.btn_mode = 1
            self.start_stop_signal.emit('stop')
        if self.btn_mode == 1:
            self.ln_btn.setText('Linkedin Getter')
            self.ln_btn.setStyleSheet(self.linkedin_btn_style)
            return
        if self.btn_mode == 2:
            self.linkedin_getter = Getter()
            self.linkedin_signal.connect(self.linkedin_getter.get_linkedin_signal)
            self.start_stop_signal.connect(self.linkedin_getter.start_stop_signal)
            self.linkedin_getter.back_signal_stop.connect(self.linkedin_finish)
            self.linkedin_getter.exception_signal.connect(self.threadExeption)

            self.linkedin_signal.emit(self.useCompanyInfo, isPremiumRequet)
            self.linkedin_getter.start()
            self.ln_btn.setText('Start')
            self.ln_btn.setStyleSheet(self.linkedin_btn_style.replace('#2196f5', 'green'))
            return
        if self.btn_mode == 3:
            self.start_stop_signal.emit('start')
            self.ln_btn.setText('Stop')
            self.ln_btn.setStyleSheet(self.linkedin_btn_style.replace('#2196f5', 'red'))
            self.btn_mode += 1
            return

    def linkedinGetterPremium(self):
        isPremiumRequet = True
        self.premium_btn_mode += 1
        if self.premium_btn_mode == 5:
            self.premium_btn_mode = 1
            self.start_stop_signal.emit('stop')
        if self.premium_btn_mode == 1:
            self.ln_premium_btn.setText('Linkedin Getter Premium')
            self.ln_premium_btn.setStyleSheet(self.linkedin_btn_style)
            return
        if self.premium_btn_mode == 2:
            self.linkedin_getter = Getter()
            self.linkedin_signal.connect(self.linkedin_getter.get_linkedin_signal)
            self.start_stop_signal.connect(self.linkedin_getter.start_stop_signal)
            self.linkedin_getter.back_signal_stop.connect(self.linkedin_finish)
            self.linkedin_getter.exception_signal.connect(self.threadExeption)
            self.linkedin_signal.emit(self.useCompanyInfo, isPremiumRequet)
            self.linkedin_getter.start()
            self.ln_premium_btn.setText('Start')
            self.ln_premium_btn.setStyleSheet(
                self.linkedin_btn_style.replace('#2196f5', 'green'))
            return
        if self.premium_btn_mode == 3:
            self.start_stop_signal.emit('start')
            self.ln_premium_btn.setText('Stop')
            self.ln_premium_btn.setStyleSheet(
                self.linkedin_btn_style.replace('#2196f5', 'red'))
            self.premium_btn_mode += 1
            return

    def linkedinGetterCompany(self):
        self.company_btn_mode += 1
        if self.company_btn_mode == 5:
            self.company_btn_mode = 1
            self.company_start_stop_signal.emit('stop')
        if self.company_btn_mode == 1:
            self.ln_company_btn.setText('Linkedin Getter Company')
            self.ln_company_btn.setStyleSheet(self.linkedin_btn_style)
            return
        if self.company_btn_mode == 2:
            self.linkedin_comapany_getter = CompanyGetter()
            self.company_start_stop_signal.connect(
                self.linkedin_comapany_getter.start_stop_signal)
            self.linkedin_comapany_getter.back_signal_stop.connect(self.linkedin_company_finish)
            self.linkedin_comapany_getter.exception_signal.connect(self.threadExeption)
            self.linkedin_comapany_getter.start()

            self.ln_company_btn.setText('Start')
            self.ln_company_btn.setStyleSheet(
                self.linkedin_btn_style.replace('#2196f5', 'green'))
            return
        if self.company_btn_mode == 3:
            self.company_start_stop_signal.emit('start')
            self.ln_company_btn.setText('Stop')
            self.ln_company_btn.setStyleSheet(self.linkedin_btn_style.replace('#2196f5', 'red'))
            self.company_btn_mode += 1
            return

    def change_is_auto_generate_emails(self, state):
        if state == Qt.Checked:
            self.useCompanyInfo = True
            self.user_store.add_or_update(storage_keys.generate_emails_checkbox, True)
        else:
            self.useCompanyInfo = False
            self.user_store.add_or_update(storage_keys.generate_emails_checkbox, False)

    def show_feedback_in_db(self):
        if self.update_mailing_list.isRunning():
            self.popUpError('Please try later')
            return
        feedback_path = QFileDialog.getOpenFileName(self,
                                                    'Choose Feedback File', "feedback/feedback", '*.xlsx')[0]
        if not feedback_path:
            return
        self.progress = ProgressBar(self)
        self.progress.show()
        self.progress.setValue(0)

        self.feedback_path_signal.emit(feedback_path)
        self.show_review.progres_signal.connect(self.load_progress_bar)
        self.show_review.start()
        self.show_review.exec()

    @pyqtSlot(int)
    def load_progress_bar(self, progress_size):
        if progress_size == -1:
            self.progress.onCancel(100)
        self.progress.setValue(progress_size)

    def show_sent_emails_in_db(self):
        if not self.active_msg_db_name:
            logging.warning('Main:show_sent_emails_in_db. No active msg db')
            return

        self.update_mailing_list.start()

    def choose_database(self):
        db_path = QFileDialog.getOpenFileName(self, 'Open File', "database/Directory", '*.xlsx')
        if not db_path[0]:
            logging.info('Main:choose_database. No selected db')
            return

        logging.info(f'Main:choose_database. Selected db: {db_path[0]}')
        self.user_store.add_or_update(storage_keys.db_path, db_path[0])
        db_name = os.path.basename(db_path[0])
        self.db_labels_list.setStyleSheet('color:green')
        self.db_labels_list.setText(f'Selected database: {db_name} {" " * 100}')

    def extract_feedback(self):
        if self.extract_outlook_feedback.isRunning():
            logging.warn("Main:extract_feedback. Already extracting")
            return
        self.extract_feedback_btn.setStyleSheet(
            self.linkedin_btn_style.replace('#2196f5;', 'red;'))
        self.extract_feedback_btn.setText('Wait...')
        self.extract_outlook_feedback.start()

    def extract_from(self):
        file = paths.extract_from
        if os.path.isfile(file):
            open_file(file)
        else:
            self.popUpError('File not found')

    def init_email_management(self):
        email_service = self.user_store.get(storage_keys.email_service)
        if email_service == email_service_outlook and is_windows():
            self.email_sender = SendEmailOutlook()
            self.radiobuttonOutlook.setChecked(True)

        else:
            self.email_sender = SendEmailSMTP()
            self.radiobuttonSMTP.setChecked(True)

        self.email_sender.finished.connect(self.stop_emailing)
        self.email_sender.sent_count_signal.connect(self.update_sent_emails_info)
        self.email_sender.exception_signal.connect(self.threadExeption)
        self.email_stop_signal.connect(self.email_sender.stop_signal)

    def switch_email_service(self):
        if self.radiobuttonSMTP.isChecked():
            logging.info('Main:switch_email_service. Using smtp')
            self.user_store.add_or_update(storage_keys.email_service, email_service_smtp)
        else:
            logging.info('Main:switch_email_service. Using outlook')
            self.user_store.add_or_update(storage_keys.email_service, email_service_outlook)

        self.init_email_management()

    def check_email_format(self):
        if os.path.isfile(paths.company_info):
            open_file(paths.company_info)
        else:
            self.popUpError('Email format file not found')

    def generate_emails(self):
        self.popUpError('Email Generator not available')
        return
        self.linkedin_generator.start()

    def start_sending_email(self):
        if self.send_email_btn_text == 'Send emails':
            self.sent_emails_count = -1

            self.send_email_btn_text = 'Stop'
            self.send_email_btn.setText(self.send_email_btn_text)
            self.send_email_btn.setStyleSheet(
                self.send_email_btn_style.replace('#2196f5', 'yellow').replace('#51a9f0', 'yellow'))

            self.email_stop_signal.emit(False)
            self.email_sender.start()
            logging.info('Main:start_sending_email. start email sender')
        else:
            self.stop_emailing()

    def stop_emailing(self):
        if self.send_email_btn_text == 'Send emails':
            return
        self.send_email_btn_text = 'Send emails'
        self.send_email_btn.setDisabled(False)
        self.send_email_btn.setText(self.send_email_btn_text)
        self.send_email_btn.setStyleSheet(self.send_email_btn_style)
        logging.info('Main:stop_emailing. stop email sender')
        if self.email_sender.isRunning():
            self.send_email_btn_text = 'Stopping...'
            self.send_email_btn.setText(self.send_email_btn_text)
            disabled_style = self.send_email_btn_style.replace('#2196f5', '#bfbdbd').replace('#51a9f0', '#bfbdbd')
            self.send_email_btn.setStyleSheet(disabled_style)
            self.send_email_btn.setDisabled(True)
            self.email_stop_signal.emit(True)
        self.show_sent_emails_in_db()

    @pyqtSlot(int, int)
    def update_sent_emails_info(self, sent_emails_count, sender_email_position):
        # prevent multiple signal emits
        if self.sent_emails_count == sent_emails_count:
            return
        self.sent_emails_count = sent_emails_count

        sent_emails_count_from_db = self.email_details.get_active_db_activity(self.active_msg_db_name)
        sent_emails_count_from_db_day = self.email_details.get_active_db_day_activity(self.active_msg_db_name)
        try:
            sender_email = self.email_sender_accounts[sender_email_position]['email']
            sent_emails_count_from_active_sender = self.email_details.get_required_email_day_activity(
                self.active_msg_db_name, sender_email)
            self.email_sender_labels[sender_email_position].setText(
                f'{sender_email}: {sent_emails_count_from_active_sender}')
        except:
            logging.error(
                f'Main:update_sent_emails_info. Can not find email sender in list {self.email_sender_accounts}, {sender_email} ')

        self.update_emailing_labels(sent_emails_count_from_db, sent_emails_count_from_db_day)

        # find out why is this 'if' for?
        if self.email_sender.isFinished():
            self.stop_emailing()

    def show_sent_emails_ui(self):
        for email_sender_label in self.email_sender_labels:
            self.scroll_layout.removeWidget(email_sender_label)
        self.email_sender_labels = []
        for accounts_dict in self.email_sender_accounts:
            sent_emails_count = self.email_details.get_required_email_day_activity(self.active_msg_db_name,
                                                                                   accounts_dict['email'])
            label = QLabel(f"{accounts_dict['email']}: {sent_emails_count}")
            label.setFont(self.info_labels_font)
            label.setStyleSheet('color:white')
            self.scroll_layout.addWidget(label)
            self.email_sender_labels.append(label)

        sent_emails_count = self.email_details.get_active_db_activity(self.active_msg_db_name)
        sent_emails_day_count = self.email_details.get_active_db_day_activity(self.active_msg_db_name)
        self.update_emailing_labels(sent_emails_count, sent_emails_day_count)

    def update_emailing_labels(self, count, daily_count):
        self.sent_email_count_label.setText(f'Sent from current DB: {count}      ')
        self.day_sent_email_count_label.setText(f'Sent today from current DB: {daily_count}      ')

    def change_message_database_path(self):
        file_path = QFileDialog.getOpenFileName(self, 'Choose')
        self.msg_db_location_label.setText(file_path[0])
        self.active_msg_db_name = file_path[0]
        self.user_store.add_or_update(storage_keys.message_db_path, file_path[0])
        self.show_sent_emails_ui()

    def edit_my_accounts(self):
        if os.path.isfile(paths.sender_accounts):
            open_file(os.path.normpath(paths.sender_accounts))
        else:
            self.popUpError('Accounts file not found')

    def customize_email(self):
        if os.path.isfile(email_template):
            open_file(email_template)
        else:
            self.popUpError('Email file not found')

    def attach_file(self):
        file_path = QFileDialog.getOpenFileNames(self, 'Choose')
        if isinstance(file_path[0], list) and len(file_path[0]) > 1:
            self.attached_file_label.setText(f"Attached: {len(file_path[0])} files {100 * ' '}")
        elif file_path[0]:
            self.attached_file_label.setText(f"Attached: {file_path[0][0]} {100 * ' '}")
        else:
            self.attached_file_label.setText(f"{200 * ' '}")
        self.user_store.add_or_update(storage_keys.attachments_paths, file_path[0])

    @pyqtSlot(str, str)
    def threadExeption(self, exception, message):
        print(exception)
        logging.error(f'Main:popUpError. Error message: {exception}')
        SaveError.save_error_msg(exception)
        self.popUpError(message)

    def popUpError(self, error_message):
        logging.error(f'Main:popUpError. Error message: {error_message}')
        QMessageBox.question(self, 'Error message', error_message, QMessageBox.Ok)

    @pyqtSlot(str)
    def popUpInfo(self, message):
        pop = ErrorDialog(self, message)
        pop.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawRectanglesAndLines(qp)
        qp.end()

    def drawRectanglesAndLines(self, qp):
        pen1 = QPen(Qt.white, 3, Qt.SolidLine)
        pen2 = QPen(Qt.white, 1, Qt.SolidLine)
        qp.setPen(pen1)
        qp.drawLine(596, 0, 596, 1200)
        qp.drawLine(604, 0, 604, 1200)
        qp.setPen(pen2)
        qp.drawLine(0, 350, 596, 350)
        qp.drawLine(604, 350, 1200, 350)

    def closeEvent(self, event):
        if self.extract_outlook_feedback.isRunning():
            self.terminate_feedback_extractor.connect(
                self.extract_outlook_feedback.terminate_process)
            self.terminate_feedback_extractor.emit()


class ExtractFeedback(QThread):
    exception_signal = pyqtSignal(str, str)
    user_store = UserStore()

    @pyqtSlot()
    def terminate_process(self):
        self.p.terminate()

    def run(self):
        try:
            email_service = self.user_store.get(storage_keys.email_service)
            if (email_service == email_service_smtp):
                self.extractor = FeedbackExtractorOutlook()
            else:
                self.extractor = FeedbackExtractorOutlook()

            if __name__ == '__main__':
                mp.set_start_method('spawn')
                self.p = mp.Process(target=self.extractor.get_attributes)
                self.p.start()
            while True:
                time.sleep(5)
                if not self.p.is_alive():
                    break
        except:
            tb = str(traceback.format_exc())
            self.exception_signal.emit(tb, ('Can not extract feedback.'
                                            'PLease make sure outlook working properly'))


def main():
    logging.info('************************************************************************')
    logging.info('********************main: start new session*************************')
    logging.info(f'***********main: Envirement={env} - debug={debug}***********')
    logging.info('************************************************************************')
    app = QApplication(sys.argv)
    view = Main()
    view.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

import time
import json
import os
import logging

import constants

class UserStore:
    def __init__(self):
        if not os.path.isfile(constants.user_store):
            self._save({})

    def add_or_update(self, name, value):
        data = self._load()
        data[name] = {"value": value, "timestamp": int(time.time())}
        self._save(data)
        logging.info(f'UserStore::add_or_update: Updating config - {name}')

    def get(self, name):
        # TODO: add defaults if not extisits
        logging.info(f'UserStore::get: Geting config - {name}')
        data = self._load()
        value = ""
        if (name in data):
            value = data[name]["value"]
        else:
            value = constants.storage_default_values[name]
        return value

    def remove(self, name):
        logging.info(f'UserStore::remove: Removing config - {name}')
        data = self._load()
        del data[name]
        self._save(data)
        return True

    def _load(self):
        with open(constants.user_store) as f:
            data = json.load(f)
        return data
    
    def _save(self, data):
        with open(constants.user_store, 'w') as f:
            json.dump(data, f, indent=4)

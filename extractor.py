import datetime
import time
import re
import os
import json
import traceback
import copy
import logging

import openpyxl
from openpyxl.styles import PatternFill
from xlsxwriter.workbook import Workbook
from PyQt5 import QtCore
from PyQt5.QtWidgets import (QDialog, QProgressBar,
                            QVBoxLayout, QHBoxLayout)

from linkedin.getter import Getter
from store import UserStore
from constants import storage_keys
from utlity import is_mac
from constants import paths
class ProgressBar(QDialog):
    def __init__(self,parent = None):
        super(ProgressBar, self).__init__(parent)
        self.resize(350,100)
        self.setWindowTitle(self.tr("Review Progress"))
        self.FeatProgressBar = QProgressBar(self)
        self.FeatProgressBar.setMinimum(0)
        self.FeatProgressBar.setMaximum (100) 
        self.FeatProgressBar.setValue (0)
        FeatLayout = QHBoxLayout()
        FeatLayout.addWidget(self.FeatProgressBar)
        layout = QVBoxLayout()
        layout.addLayout(FeatLayout)
        self.setLayout(layout)
        self.show()

    def setValue(self,value):
        self.FeatProgressBar.setValue(value) 
 
    def onCancel(self,event):
        self.close()

class FeedbackExtractorSMTP:
    def get_attributes(self):
        print('extracting from smtp')

class FeedbackExtractorOutlook:
    
    def __init__(self,last_receive_time=False):
        self.last_receive_time=last_receive_time

    def get_attributes(self):
        if is_mac():
            logging.warn('FeedbackExtractorOutlook:get_attributes. is mac os, passing')
            return 
        import win32com.client
        self.outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")	
        self.accounts = win32com.client.Dispatch("Outlook.Application").Session.Accounts;	
        # required_folder_name = 'Elementos Eliminados'
        # self.required_folder_name='Inbox'
        self.extract_from_accounts,self.required_folder_name=self.get_extractor_details()
        self.data_dicts_list=[]
        self.get_folders()
        self.save_emails_feedback()

    def get_extractor_details(self):
        try:
            with open(paths.extract_from) as file:
                details=file.read()
            details_list=details.split()
            required_folder_name=details_list[1]
            extract_from_accounts=details_list[3:]
        except Exception as e:
            extract_from_accounts, required_folder_name = [], ''
            message = 'Can not read extract from'
            logging.error(f"FeedbackExtractorOutlook::get_extractor_details: {message}", exc_info=True)
        return extract_from_accounts, required_folder_name

    def get_folders(self):
        for account in self.accounts:
            account_name=account.DisplayName
            if account_name not in self.extract_from_accounts:
                continue
            try:
                inbox = self.outlook.Folders(account.DeliveryStore.DisplayName)
                self.folders = inbox.Folders
            except Exception as e:
                logging.error('FeedbackExtractorOutlook:get_folders.', exc_info=True)
                self.folders = []#self.outlook.GetRootFolder()
            self.get_items()

    def get_items(self):
        for folder in self.folders:
            print(folder.name)
            if self.required_folder_name not in folder.name:
                continue
            self.get_messages_attributes(folder)

    def get_messages_attributes(self,folder):
        messages = folder.Items
        
        # undelivered_list=['eturned', 'ndeliver', 'o se puede entregar', 
        # "ouldn't be delivered", 'ailure', 'in entregar']
        try:
            print(messages.Count)
        except:
            pass
        m=0
        for message in messages:
            try:
                receive_time=str(message.ReceivedTime.time())
                receive_date=str(message.ReceivedTime.date())
                receive_time = receive_time.split(':')
                receive_date = receive_date.split('-')
                dt = datetime.datetime(int(receive_date[0]), int(receive_date[1]),
                int(receive_date[2]), int(receive_time[0]), int(receive_time[1]),
                int(receive_time[2]))
                receive_datetime=int(time.mktime(dt.timetuple()))
                if self.last_receive_time:
                    if receive_datetime<self.last_receive_time:
                        continue
            except:
                pass

            m+=1
            data_dict={}
            undeliver_data_dict={}
            message_subject=str(message.Subject)
            subject_first=str(message_subject)
            if ('eturn' not in subject_first and 'ndeliver' 
                not in subject_first and 'o se puede entregar' not in 
                subject_first and "ouldn't be delivered" not in 
                subject_first and 'Fail' not in subject_first
                and 'in entregar' not in subject_first and 'fail' not in subject_first and
                'ntrega retrasada' not in subject_first and 'FAIL' not in subject_first):
                try:
                    data_dict['Name']=message.SenderName
                except:
                    data_dict['Name']=''
                try:
                    data_dict['Email']=message.SenderEmailAddress
                except Exception as e:
                    data_dict['Email']=''
            else:
                message_body=message.body
                emails=re.search(r'[\w\.-]+@[\w\.-]+', message_body)
                data_dict['Name']=''
                try:
                    data_dict['Email']=emails[0].strip('.').strip("'") 
                except:
                    data_dict['Email']=''
                # message2.Delete()
            data_dict['Feedback']=message_subject
            self.data_dicts_list.append(data_dict)
        # self.save_emails_feedback()


    def save_emails_feedback(self):
        dt_time = datetime.datetime.now()
        dt_string=dt_time.strftime("%d-%m_%H-%M")
        file_location='feedback/feedback_{}.xlsx'.format(dt_string)
        workbook = Workbook(file_location)
        worksheet = workbook.add_worksheet()
        for i,each_dict in enumerate(self.data_dicts_list):
            worksheet.write(i, 0, each_dict['Name'])
            worksheet.write(i, 1, each_dict['Email'])
            worksheet.write(i, 2, each_dict['Feedback'])
        workbook.close()

    def get_xlsx_feedback(self,db_path):
        if not os.path.isfile(db_path):
            return []
        book = openpyxl.load_workbook(db_path, data_only=True)
        sheets=book.sheetnames
        sheet = book[sheets[0]]
        rows = sheet.rows
        key_list=['full_name', 'email', 'feedback']
        values_dict_list=[]
        for row in rows:
            values_dict={}
            for i,cell in enumerate(row):
                if i >= len(key_list):
                    continue
                if cell.value:
                    values_dict[key_list[i]]=cell.value
                else:
                   values_dict[key_list[i]]='' 
            values_dict_list.append(values_dict)
        book.close()
        return values_dict_list


class ShowReview(QtCore.QThread):

    progres_signal=QtCore.pyqtSignal(int)
    exception_signal=QtCore.pyqtSignal(str, str)
    user_store = UserStore()

    @QtCore.pyqtSlot(str)
    def path_signal(self,feedback_path):
        self.feedback_path=feedback_path

    def run(self):
        try:
            self.show_feedback_in_db()
        except:
            tb=str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Can not show feedback in database. Please check databases')
            return 

    def show_feedback_in_db(self):
        db_path=self.user_store.get(storage_keys.db_path)
        getObj=Getter()
        getObj.db_path=db_path
        employee_list=getObj.read_old_database()
        tutuz=FeedbackExtractorOutlook()
        feedback_list=tutuz.get_xlsx_feedback(self.feedback_path)
        # if os.path.isfile(db_path):
        #     os.remove(db_path)
        if not feedback_list:
            self.progres_signal.emit(-1)
            raise('Extracted feedbacks not found')
        if not employee_list:
            self.progres_signal.emit(-1)
            raise('Employees data not found')
        sorted_feedback_dicts_list = sorted(feedback_list,key=lambda k: k['email']) 
        sorted_employee_dicts_list = sorted(employee_list,key=lambda k: k['email']) 
        reviewed_employees_dicts_list=[]
        progress_size=len(sorted_feedback_dicts_list)//100
        if progress_size==0:
            progress_size=1
        temp_feedback_dicts_list=copy.deepcopy(sorted_feedback_dicts_list)
        feedback_index_list=[i for i in range(len(sorted_feedback_dicts_list)+1)] 
        for i,feedback_dict in enumerate(sorted_feedback_dicts_list):
            isCompareCharGone=False
            for j,employee_dict in enumerate(sorted_employee_dicts_list):
                if (not feedback_dict or not employee_dict or feedback_dict['email']==''
                     or employee_dict['email']==''):
                    continue
                if employee_dict['email'][0]==feedback_dict['email'][0] and not isCompareCharGone:
                    isCompareCharGone=True
                elif employee_dict['email'][0]!=feedback_dict['email'][0] and isCompareCharGone:
                    break
                if employee_dict['email'].lower() == feedback_dict['email'].lower():
                    sorted_employee_dicts_list.remove(employee_dict)
                    # temp_feedback_dicts_list.remove(feedback_dict)
                    if employee_dict['email'] == feedback_dict['email']:
                        qk=feedback_list.index(feedback_dict)
                        feedback_list[qk]=-1
                        feedback_index_list.remove(qk)
                    feedback=feedback_dict['feedback']
                    employee_dict['feedback_status']=feedback
                    if ('eturn' in feedback or 'ndeliver' in feedback
                            or 'o se puede entregar' in feedback 
                            or "ouldn't be delivered" in feedback 
                            or 'ail' in feedback or 'FAIL' in feedback
                            or 'in entregar' in feedback or 'ntrega retrasada' in feedback):
                        employee_dict['feedback']='Undelivered'
                    elif ('read' in feedback or 'eliver' in feedback or 'ucces' in feedback
                            or 'Read' in feedback or 'Lu' in feedback):
                        employee_dict['feedback']='Delivered'
                    # else:
                        # print(employee_dict['email'],'[:unexpected feedback:]',feedback)
                    reviewed_employees_dicts_list.append(employee_dict)
                    break

        

            if i % progress_size == 0:
                if progress_size == 1:
                    progress = 100
                else:
                    progress = i / progress_size
                self.progres_signal.emit(progress)
        # reviewed_employees_dicts_list=list(unique_everseen(reviewed_employees_dicts_list))
        reviewed_employees_dicts_list += sorted_employee_dicts_list
        reviewed_employees_dicts_list = sorted(reviewed_employees_dicts_list,
                                                    key=lambda k: k['email']) 
        try:
            self.show_unfound_records_in_feedback(feedback_index_list,sorted_feedback_dicts_list)    
            getObj.updateDatabase(reviewed_employees_dicts_list)
        except Exception as e:
            logging.error('FeedbackExtractorOutlook:show_feedback_in_db.', exc_info=True)
            print('show_feedback_in_db: ', e)
            tb=str(traceback.format_exc())
            self.exception_signal.emit(tb, 'Please make sure databases are available and closed')
        self.progres_signal.emit(-1)

    def show_unfound_records_in_feedback(self, feedback_index_list,feedback_dicts_list):
        feedback_file=openpyxl.load_workbook(self.feedback_path)
        ws=feedback_file.worksheets[0]
        for i in feedback_index_list:
            try:
                greenFill = PatternFill(start_color='00FF00',
                   end_color='00FF00',
                   fill_type='solid')
                email_cell=ws[f'B{i+1}'].value
                if email_cell=='' or email_cell==None:
                    greenFill = PatternFill(start_color='FFFF00',
                    end_color='FFFF00',
                    fill_type='solid')
                ws[f'A{i+1}'].fill = greenFill
                ws[f'B{i+1}'].fill = greenFill
                ws[f'C{i+1}'].fill = greenFill
            except:
                pass
        feedback_file.save(self.feedback_path)
        feedback_file.close()
        